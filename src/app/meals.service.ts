import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
 
export interface Meal {
  id?: string;
  food: string;
  quantity: string;
  time: string;
  before: string;
  after: string; 
}
 
@Injectable({
  providedIn: 'root'
})
export class MealsService {
  private mealsCollection: AngularFirestoreCollection<Meal>;
 
  private meals: Observable<Meal[]>;
 
  constructor(db: AngularFirestore) {
    this.mealsCollection = db.collection<Meal>('meals');
 
    this.meals = this.mealsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  getMeals() {
    return this.meals;
  }
 
  getMeal(id) {
    return this.mealsCollection.doc<Meal>(id).valueChanges();
  }
 
  updateMeal(meal: Meal, id: string) {
    return this.mealsCollection.doc(id).update(meal);
  }
 
  addMeal(meal: Meal) {
    return this.mealsCollection.add(meal);
  }
 
  removeMeal(id) {
    return this.mealsCollection.doc(id).delete();
  }
}
