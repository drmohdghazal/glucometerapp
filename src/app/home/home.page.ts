import { Component } from '@angular/core';
import { Hotspot, HotspotNetwork } from '@ionic-native/hotspot/ngx';
import { TextToSpeech } from '@ionic-native/text-to-speech/ngx';


import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import 'rxjs/add/observable/interval';


export interface WifiSignature {
  id?: string;
  location: string;
  fingerprint: Array<HotspotNetwork>;
}


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  Direction:string;
  networkList:Array<HotspotNetwork>; // for mapping by developer
  unknownList:Array<HotspotNetwork>; // for localizing  by regular user

 
  private locationsCollection: AngularFirestoreCollection<WifiSignature>;
  private locations: Observable<WifiSignature[]>;

  locationName:string; // Stores the location name by the developer while mapping
  foundLocation:string;// Used to display the location found to the regular using while localizing.

  database:any[]; // Used to store a local copy of the database


  // Two providers/plugins/services
  // hotspot: to do wifiscans
  // db: to access the database cloud
  constructor(private hotspot: Hotspot, private db: AngularFirestore,private tts: TextToSpeech) {



    this.tts.speak("You are in the BioImaging Lab. The room contains several work benches and equipment. There are posters on the wall.  There is a person to the left. There is a person near the presentation area. There is a person in the middle of the room. There is a desk at the south of the room. There is a desk at the south of the room. There is a desk to the right of the room. The room temperature is 22 degrees. Relative humidity level is 45%. The lights are on. Light intensity is high. ")

    this.Direction = ""
    //An initial scan for elnazer
    this.wifiscan();
    this.locationName = "";
    this.foundLocation = "Unknown";
/*
    this.locationsCollection = db.collection<WifiSignature>('locations');
 
    this.locations = this.locationsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
     Observable.interval(1000).subscribe(x => {
      //this.locate();
    });
    */
  }
  wifiscan():void{
    /*
      I am calling a function in my provider hotspot. This function is called scanWifi().
      It returns a promise. I can use .then() to tell it what to do when the promise is fullfilled.
      Promise fullfilled is fancy for the data is now available. I supply a function to call.
      This function is (networks) => {..}. The networks variable is actually of type
      Array<HotspotNetwork>, which is a list of HotspotNetworks. A HotspotNetwork is made out of
      an SSID (could be repeated), BSSID (unique mac), and a level (-40 (good) to -100 (bad)).

      1. Print it to the screen.
      2. Sort this list by mac address.
      3. Save it in my local networkList variable sorted.

     */


    this.hotspot.scanWifi().then((networks: Array<HotspotNetwork>) => {
      console.log(networks);
      networks.sort(this.compareBSSID);
      this.networkList = networks;
    });
  }

  storescan():void{
    let location = { location:this.locationName,fingerprint: this.networkList };
    this.locationsCollection.add(location);
  }

  locate():void{
    this.hotspot.scanWifi().then((networks: Array<HotspotNetwork>) => {

      // Do a scan and then store it in the unknownList (because it is the user not the developer
      // using this part
      networks.sort(this.compareBSSID);
      this.unknownList = networks;

      
      // Download the database of locations from firebase.
      this.db.collection<WifiSignature>('locations').valueChanges().subscribe(data=>{

        // Do you have locations, or did the developer not create the map yet.
          if(data.length != 0) {

            // Store local copy of database

            // AIzaSyCVhJw2dB6n-pFsXgqQcbVImXTFn7CIK4A
            this.database = data;

            // Display it for debugging on the console. Can be removed in production.
            console.log(data);

            // Find out the distance (similarity) between the wifi fingerprint of the unknown
            // location and each of the wifi fingerprints (known) stored in the database
            // Do that by calling goOverSignature()
            let distances: number[] = this.goOverSignatures(this.unknownList);
            console.log("Distances " + distances);

            // Minimum distance finding code taught in CSC201!
            let minIndex = 0;
            let min = distances[0];
            for (let i = 1; i < distances.length; i++) {
              if (distances[i] < min) {
                minIndex = i;
                min = distances[i];
              }
            }

            // The location with the minimum distance (maximum similiarity) is declared the
            // location of the user (foundLocation).
            this.foundLocation = this.database[minIndex].location;
            
            if(this.foundLocation == "Room")
              {
                this.Direction = "اتجه يمينا";
                this.tts.speak("Turn Right")
                  .then(() => console.log('Success'))
                  .catch((reason: any) => console.log(reason));
              }
            else if(this.foundLocation == "Corridor")
            {
              this.Direction = "سر للأمام";
              this.tts.speak("Go Forward")
              .then(() => console.log('Success'))
              .catch((reason: any) => console.log(reason));
            }
            else if(this.foundLocation == "Salon")
              {
                this.Direction = "اتجه يسارا";
                this.tts.speak("Now Turn Left")
                  .then(() => console.log('Success'))
                  .catch((reason: any) => console.log(reason));
              }
            else
              this.Direction = "Await Instructions";
            


          }
          else {
            this.foundLocation = "Empty Database";
          }
        });

    });
  }

  goOverSignatures(unknownSignature):number[]{

    // Declare and create an empty list of distances (similarity measures)
    let distances:number[];
    distances = [];

    // Go over entire database (linear search)
    for ( let i = 0 ; i < this.database.length; i++)
      // Compare the fingerprint of the unknown location with all fingerprints in the database
      // return the distance and store it in the list of distances.
      distances.push(this.compare(unknownSignature, this.database[i].fingerprint));

    //return the list to the caller (locate()
    return distances;
  }
  compareBSSID(signature1,signature2):number{
    return signature1.BSSID.localeCompare(signature2.BSSID);
  }
  compare(signature1,signature2):number{
    let thisListSize = signature1.length;
    let otherListSize = signature2.length;

    let count = 0; // Number of matched WifiSignature
    let sumOfDifference = 0;
    // Sum of square of each difference of each matched WifiSignature's RSS
    let idThisList = 0;
    let idOtherList = 0;
    while (idThisList < thisListSize && idOtherList < otherListSize) {
      let thisSignature = signature1[idThisList];
      let anotherSignature = signature2[idOtherList];
      let compareResult = thisSignature.BSSID.localeCompare(anotherSignature.BSSID);
      if (compareResult == 0) {
        // only compare the level of the same wifi address
        count++;
        let difference = thisSignature.level
          - anotherSignature.level;
        sumOfDifference += difference * difference;
        idThisList++;
        idOtherList++;
      } else if (compareResult < 0) {
        idThisList++;
      } else {
        idOtherList++;
      }
    }
    let proportion:number = count / Math.min(thisListSize, otherListSize);
    let averageDifference:number= sumOfDifference / count;
    let result:number;
    if (proportion < 0.5) {
      result = 120+1;
    } else {
      result = averageDifference / proportion;
    }
    return result;
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad WififingerprintingPage');
  }

}
